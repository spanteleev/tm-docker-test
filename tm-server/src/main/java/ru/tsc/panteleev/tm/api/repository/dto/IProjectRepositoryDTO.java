package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    void set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

    void clearByUserId(@NotNull String userId);

    void clear();

    long getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

}
