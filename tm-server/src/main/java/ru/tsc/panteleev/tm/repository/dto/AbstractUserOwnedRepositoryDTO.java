package ru.tsc.panteleev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

}
